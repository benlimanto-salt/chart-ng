import { Component, OnInit } from '@angular/core';

import * as Highcharts from 'highcharts';
import exporting from 'highcharts/modules/exporting';
import offline from 'highcharts/modules/offline-exporting';

@Component({
  selector: 'app-chart-highchart',
  templateUrl: './chart-highchart.component.html',
  styleUrls: ['./chart-highchart.component.scss']
})
export class ChartHighchartComponent implements OnInit {
  ngOnInit(): void {
    // @see https://stackoverflow.com/a/55959862/4906348
    exporting(this.Highcharts);
    offline(this.Highcharts);
  }

  public Highcharts: typeof Highcharts = Highcharts; // required

  public chartOptions: Highcharts.Options = {
    title: {
      text: "Perkembangan Siswa"
    },
    series: [
      {
        type: 'line',
        data: [1,2,5,3,4,7],
        name: 'Siswa'
      }
    ],
    xAxis: {
      categories: ['1', '2', '3', '4', '5', '6'],
      title: {
        text: "Periode"
      }
    },
    yAxis: {
      title: {
        text: "Jumlah"
      }
    }
  }
}
