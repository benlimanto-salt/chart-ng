import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartHighchartComponent } from './chart-highchart.component';

describe('ChartHighchartComponent', () => {
  let component: ChartHighchartComponent;
  let fixture: ComponentFixture<ChartHighchartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChartHighchartComponent]
    });
    fixture = TestBed.createComponent(ChartHighchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
