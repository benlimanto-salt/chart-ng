import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartViewComponent } from './chart-view/chart-view.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { CanvasJSAngularChartsModule } from '@canvasjs/angular-charts';
import { ChartHighchartComponent } from './chart-highchart/chart-highchart.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { ChartViewNg2chartComponent } from './chart-view-ng2chart/chart-view-ng2chart.component';
import { NgChartsModule } from 'ng2-charts';

const router: Routes = [
  { path: '', component: ChartViewComponent},
  { path: 'highchart', component: ChartHighchartComponent},
  { path: 'chartjs', component: ChartViewNg2chartComponent}
];

@NgModule({
  declarations: [
    ChartViewComponent,
    ChartHighchartComponent,
    ChartViewNg2chartComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(router),
    CanvasJSAngularChartsModule,
    HighchartsChartModule,
    NgChartsModule
  ]
})
export class ChartPageModule { }
