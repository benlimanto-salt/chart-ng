import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartViewNg2chartComponent } from './chart-view-ng2chart.component';

describe('ChartViewNg2chartComponent', () => {
  let component: ChartViewNg2chartComponent;
  let fixture: ComponentFixture<ChartViewNg2chartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChartViewNg2chartComponent]
    });
    fixture = TestBed.createComponent(ChartViewNg2chartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
