import { Component, EventEmitter, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ChartConfiguration, Plugin } from 'chart.js';
import { ChartEvent } from 'chart.js/dist/core/core.plugins';
import { LegendOptions } from 'highcharts';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-chart-view-ng2chart',
  templateUrl: './chart-view-ng2chart.component.html',
  styleUrls: ['./chart-view-ng2chart.component.scss']
})
export class ChartViewNg2chartComponent implements OnInit {

  ngOnInit(): void {
    
  }

  @ViewChildren(BaseChartDirective) chart?: QueryList<BaseChartDirective>;

  public lineChartData: ChartConfiguration['data'] = {
    datasets: [
      {
        data: [65, 59, 80, 81, 56, 55, 40],
        label: 'Series A',
        backgroundColor: 'rgba(255,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)',
        fill: 'origin',
      },
      {
        data: [10, 30, 10, 31, 56, 28, 40],
        label: 'Series B',
        backgroundColor: 'rgba(100,159,177,0.2)',
        borderColor: 'rgba(200,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)',
        fill: 'origin',
      },
    ],
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July']
  };

  public lineChartOptions: ChartConfiguration['options'] = {
    plugins: {
      title: {
        display: true,
        text: "Chart Line",
        font: {
          size: 20
        }
      }
    }
  }

  public GetOptions(newTitle: string): ChartConfiguration['options']
  {
    return {
      events: ['click'],
      plugins: {
        title: {
          display: true,
          text: newTitle,
          font: {
            size: 20
          }
        }
      }
    };
  }

  public clickChart(e?: {
    event?: ChartEvent;
    active?: Array<any>;
  })
  {
    console.log(e?.active); // This is the data!
    let x = e?.active?.at(0)?.datasetIndex ?? 0;
    let y = e?.active?.at(0)?.index ?? 0;
    let label = (this.lineChartData.labels as Array<any>)[y] ?? "";

    alert(label + " - " + this.lineChartData.datasets[x].data[y]?.toString());
    console.log(e?.event?.native ?? "");
  }

  /**
   * Hard way of implementing click?
   * @returns array of plugins with click even
   */
  public GetPlugins(): Plugin[]
  {
    return [
      {
        id: "onClickEvent",
        beforeEvent: (chart, args, options) => {
          const evt = args.event;
          if (evt.type == "click")
          {
            console.log(evt);
            console.log(chart);
            console.log(options);
            console.log(chart.getElementsAtEventForMode(evt as any, 'nearest', {intersect: true}, true));
            let active = chart.getElementsAtEventForMode(evt as any, 'nearest', {intersect: true}, true);
            let x = active?.at(0)?.datasetIndex ?? 0;
            let y = active?.at(0)?.index ?? 0;

            let label = (this.lineChartData.labels as Array<any>)[y] ?? "";
        
            alert(label + " - " + this.lineChartData.datasets[x].data[y]?.toString());
          }
        },
      }
    ];
  }

  public lineChartType: ChartConfiguration['type'] = 'line';
  
  public AddData()
  {
    this.lineChartData.datasets[0].data.push(Math.floor(Math.random()*100));
    this.lineChartData.labels?.push("a"+(Math.random()).toString());
    console.log(this.lineChartData);
    // @see https://stackoverflow.com/a/40165639/4906348
    // Multiple children with same directive/selector!
    console.log(this.chart?.toArray().forEach(i => {
      if (i.options?.plugins?.title?.text == "Chart Line")
      {
        this.lineChartOptions = this.GetOptions("aaaa");
      }
      
      i.chart?.update();
      console.log(i.chart?.options.plugins?.title);
    }));
  }

  public ExportAsPNG()
  {
    let canvasCtx = (this.chart?.toArray()[0].ctx as any) as CanvasRenderingContext2D;
    let img = canvasCtx.canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");

    let a = document.createElement("a");
    a.href = img;
    a.download = "Chart-Export.png";
    a.click();
  }
}
