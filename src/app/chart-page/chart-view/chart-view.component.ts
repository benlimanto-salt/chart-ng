import { AfterViewChecked, AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-chart-view',
  templateUrl: './chart-view.component.html',
  styleUrls: ['./chart-view.component.scss']
})
export class ChartViewComponent implements AfterViewInit {
  
  ngAfterViewInit(): void {
    // Removing the credit?
    let dom: HTMLDivElement = this.chart.nativeElement;
    console.log(dom.querySelector(".canvasjs-chart-credit")?.remove());
  }

  @ViewChild("chart") chart!: ElementRef;
  
  public chartOptionsColumn = {
    title: "",
    animationEnabled: true,
    axisY: {
      includeZero: true
    },
    data: [
      {
        type: "column",
        indexLabelFontColour: "#00C23E",
        // @see https://canvasjs.com/docs/charts/basics-of-creating-html5-chart/event-handling/
        click: (e: any) => { 
          alert(  e.dataSeries.type+ " x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y);
        },
        dataPoints: [
          { x: 10, y: 71 },
          { x: 20, y: 55 },
          { x: 30, y: 50 },
          { x: 40, y: 65 },
          { x: 50, y: 71 },
          // @see https://canvasjs.com/docs/charts/basics-of-creating-html5-chart/event-handling/
          { x: 60, y: 92, indexLabel: "Highest\u2191", click: (e: any) => { alert( "Data Tertinggi!" ) }},
          { x: 70, y: 68 },
          { x: 80, y: 38, indexLabel: "Lowest\u2193"  },
          { x: 90, y: 54 },
          { x: 100, y: 60 }
        ]
      }
    ]
  };

  public chartOptionsLine = {
    title: "",
    animationEnabled: true,
    axisY: {
      includeZero: true
    },
    data: [
      {
        type: "line",
        indexLabelFontColour: "#00C23E",
        // @see https://canvasjs.com/docs/charts/basics-of-creating-html5-chart/event-handling/
        click: (e: any) => { 
          alert(  e.dataSeries.type+ " x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y);
        },
        dataPoints: [
          { x: 10, y: 71 },
          { x: 20, y: 55 },
          { x: 30, y: 50 },
          { x: 40, y: 65 },
          { x: 50, y: 71 },
          // @see https://canvasjs.com/docs/charts/basics-of-creating-html5-chart/event-handling/
          { x: 60, y: 92, indexLabel: "Highest\u2191", click: (e: any) => { alert( "Data Tertinggi!" ) }},
          { x: 70, y: 68 },
          { x: 80, y: 38, indexLabel: "Lowest\u2193"  },
          { x: 90, y: 54 },
          { x: 100, y: 60 }
        ]
      }
    ]
  };

}
